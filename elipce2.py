import math
import numpy as np

from matplotlib import pyplot as plt 
from math import pi 

u=3  #x-posicion del centro
v=2 #y-posicion del centro
a=4  #radio en el eje x
b=5 #radio en el eje y 

t = np.linspace(0, 2*pi, 100) 
plt.plot(u+a*np.cos(t) , v+b*np.sin(t)) 
plt.grid(color='lightgray',linestyle='--') 
plt.show() 